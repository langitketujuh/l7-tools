# L7 Tools

`l7-tools` merupakan perkakas cli untuk melakukan konfigurasi sistem seperti pembaruan, chroot mode, memasang grub, fiksasi boot order, memasang pengguna baru dan lain-lain.

Dependency:

  `upterm l7-base-files l7-opendoas`

- Memperbarui sistem agar menjadi lebih baru.

  ```
  sky --upgrade
  ```

- Memasang grub.

  ```
  sky --grub
  ```

- Masuk ke mode chroot, berguna untuk memperbaiki jika ada kernel panic atau masalah lainnya.

  ```
  sky --chroot
  ```

- Memperbaiki screen tearing dan menampilakan grub menu sistem operasi lain.

  ```
  sky --patch
  ```

- Memasang pengguna baru, berguna untuk mengatasi gagal login.

  ```
  sky --user
  ```

- Menghapus perangkat lunak yang tidak dibutuhkan.
  ```
  sky --downgrade
  ```

Anda memerlukan akses root (sudo/doas) untuk menjalankan perintah diatas. Lebih jelasnya jalankan `sky --help`.
