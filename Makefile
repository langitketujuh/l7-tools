PREFIX = /usr

all:
	@echo Run \'make install\' to install l7-tools.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@mkdir -p $(DESTDIR)$(PREFIX)/share/applications
	@cp -p sky $(DESTDIR)$(PREFIX)/bin/sky
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/sky
	@cp -p langitketujuh.system.upgrade.desktop $(DESTDIR)$(PREFIX)/share/applications/langitketujuh.system.upgrade.desktop
	@cp -p langitketujuh.remote.desktop $(DESTDIR)$(PREFIX)/share/applications/langitketujuh.remote.desktop
	@cp -p langitketujuh.chroot.desktop $(DESTDIR)$(PREFIX)/share/applications/langitketujuh.chroot.desktop

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/sky
	@rm -rf $(DESTDIR)$(PREFIX)/share/applications/langitketujuh.system.upgrade.desktop
	@rm -rf $(DESTDIR)$(PREFIX)/share/applications/langitketujuh.remote.desktop
	@rm -rf $(DESTDIR)$(PREFIX)/share/applications/langitketujuh.chroot.desktop
